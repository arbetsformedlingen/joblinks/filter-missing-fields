#!/usr/bin/env bash
set -eEu -o pipefail

filter="$(echo ${1} | sed 's|,| |g')"
filterfile="${2}"

function join_by { local d=$1; shift; local f=$1; shift; printf %s "$f" "${@/#/$d}"; }

negfilt0=$(echo "$filter" | sed -E 's|(\S+)|\1==null|g')
negfilter=$(join_by ' or ' $negfilt0) # must not be quoted

posfilt0=$(echo "$filter" | sed -E 's|(\S+)|\1!=null|g')
posfilter=$(join_by ' and ' $posfilt0) # must not be quoted

tmpfile=$(mktemp)

jq -cs '.' > "${tmpfile}"

jq '. | map(select('"${negfilter}"'))' < "${tmpfile}" | jq -c '.[]' >"${filterfile}"

jq '. | map(select('"${posfilter}"'))' < "${tmpfile}" | jq -c '.[]'

rm -f "${tmpfile}"
