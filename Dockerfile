FROM docker.io/library/ubuntu:22.04 AS base

ADD filter.sh /usr/local/bin/filter.sh

RUN apt-get -y update &&\
    apt-get -y install jq &&\
    apt-get -y clean && apt-get -y autoremove &&\
    chmod a+rx /usr/local/bin/filter.sh


###############################################################################
FROM base AS test

COPY tests/testdata/ /testdata/

RUN  filter.sh '.id,.originalJobPosting.description,.originalJobPosting.title' /tmp/removed.1 < /testdata/input.10 > /tmp/output.9 \
     && diff /testdata/expected_output.9 /tmp/output.9 \
     && diff /testdata/expected_removed.1 /tmp/removed.1 \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

ENTRYPOINT ["/usr/local/bin/filter.sh"]
